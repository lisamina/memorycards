CREATE DATABASE IF NOT EXISTS memorycards; 

-- Créer un utlisateur MySQL pour cette application 
CREATE USER IF NOT EXISTS 'sanjani'@'localhost' IDENTIFIED BY 'reshina'; 

-- Pour modifier le mot de passe d'un utilisateur MySQL (si on l'a perdu par exemple). A faire depuis le compte root. 

-- Gestion des droits : 
-- On accorde tous les droits à l'utilisateur sur la base profile 

GRANT ALL PRIVILEGES ON memorycards.* TO sanjani@localhost;
FLUSH PRIVILEGES;

USE memorycards; -- On se positionne sur la base

-- Création de la table utilisateur 

CREATE TABLE IF NOT EXISTS utilisateur ( 
    id_utilisateur INT AUTO_INCREMENT, 
    email VARCHAR(100) NOT NULL, -- email est une chaîne de caractère de longueur variable (jusqu'à 100 caractères max)
    password VARCHAR(255) NOT NULL,  -- la valeur de cette colonne doit être renseignée
    pseudo VARCHAR(30) NOT NULL,
    -- On indique quelle est la clé primaire : 
    PRIMARY KEY (id_utilisateur)
); 

-- On rajoute une contrainte d'unicité sur la colonne email
ALTER TABLE utilisateur ADD CONSTRAINT UNIQUE(email);
ALTER TABLE utilisateur ADD CONSTRAINT UNIQUE(pseudo);


CREATE TABLE IF NOT EXISTS categorie (
    id_categorie INT AUTO_INCREMENT,
    nom VARCHAR(50) NOT NULL,

    PRIMARY KEY (id_categorie)
);

ALTER TABLE categorie ADD CONSTRAINT UNIQUE (nom);


CREATE TABLE IF NOT EXISTS theme (
    id_theme INT AUTO_INCREMENT,
    id_utilisateur INT,
    id_categorie INT,
    nom_theme VARCHAR(50) NOT NULL,
    description TEXT NOT NULL,
    public BOOLEAN NOT NULL,
    date_creation DATE NOT NULL,

    PRIMARY KEY (id_theme),
    FOREIGN KEY (id_utilisateur) REFERENCES utilisateur(id_utilisateur),
    FOREIGN KEY (id_categorie) REFERENCES categorie(id_categorie)
);

ALTER TABLE theme ADD CONSTRAINT UNIQUE (nom_theme);


CREATE TABLE IF NOT EXISTS revision (
    id_revision INT AUTO_INCREMENT,
    id_utilisateur INT,
    id_theme INT, /* JOINTURE*/
    nb_niveau INT,
    nb_carte INT NOT NULL DEFAULT 1,
    date_creation DATE NOT NULL,

    PRIMARY KEY (id_revision),
    FOREIGN KEY (id_utilisateur) REFERENCES utilisateur(id_utilisateur),
    FOREIGN KEY (id_theme) REFERENCES theme(id_theme)
);

CREATE TABLE IF NOT EXISTS carte (
    id_carte INT AUTO_INCREMENT,
    id_theme INT,
    recto VARCHAR (255) NOT NULL,
    verso VARCHAR (255) NOT NULL,
    img_recto VARCHAR (255),
    img_verso VARCHAR (255),
    date_creation DATE,
    date_modification DATE NOT NULL,

    PRIMARY KEY (id_carte),
    FOREIGN KEY (id_theme) REFERENCES theme(id_theme) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS revu (
    id_revision INT,
    id_carte INT,
    derniere_vu DATE NOT NULL,
    niveau INT, 

    PRIMARY KEY (id_revision, id_carte),
    FOREIGN KEY (id_revision) REFERENCES revision(id_revision),
    FOREIGN KEY (id_carte) REFERENCES carte(id_carte)
);


